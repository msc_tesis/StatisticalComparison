ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.12"
libraryDependencies += "org.apache.commons" % "commons-math3" % "3.6.1"


lazy val root = (project in file("."))
  .settings(
    name := "CSVProcessor"
  )
