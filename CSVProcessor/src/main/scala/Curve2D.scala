import scala.collection.mutable.ArrayBuffer
import scala.math._


class Curve2D {
  val values: ArrayBuffer[(Double, Double)] = ArrayBuffer[(Double, Double)]()

  private def shortestDistance(point: (Double, Double)): (Int, Double) = {
    var shortest: Double = Double.MaxValue
    var shortestIndex: Int = 0
    values.view.zipWithIndex.foreach { case (newPoint: (Double, Double), index: Int) =>
      val x: Double = newPoint._1 - point._1
      val y: Double = newPoint._2 - point._2
      //print(s"$x , $y |")
      val newone: Double = x * x + y * y
      if (newone < shortest) {
        shortest = newone
        shortestIndex = index
      }
    }
    (shortestIndex, math.sqrt(shortest))
  }

  def frechet(other: Curve2D): Double = {
    var frechet: Double = 0
    var frechetIndex: (Int,Int) = (0,0)
    values.view.zipWithIndex.foreach { case (value: (Double, Double), index: Int) =>
      val (indexB:Int, shortDistance: Double) = other.shortestDistance(value)
      if (frechet < shortDistance){
        frechet = shortDistance
        frechetIndex=(index,indexB)
      }
    }
    frechet
  }

  def append(xy: (Double, Double)): Unit = {
    values.append(xy)
  }

  def length: Int = {
    values.length
  }

  def pointAt(point: Int): (Double, Double) = {
    values(point)
  }

}
