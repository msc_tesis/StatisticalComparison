import scala.collection.mutable.ArrayBuffer

class EntryContainer {
  var min: Double = Double.MaxValue
  var max: Double = Double.MinValue
  var total: Int = 0
  private var count: Double = 0
  val elements: ArrayBuffer[Double] = new ArrayBuffer[Double]()
  var histogram: Curve2D = _


  def addValue(value: Double): Unit = {
    count += value
    total += 1
    if (value < min) {
      min = value
    }
    if (value > max) {
      max = value
    }
    elements.append(value)
  }

  def avg: Double = {
    count / total
  }

  def variance: Double ={
    var average = avg
    var varianceValue: Double = 0
    elements.foreach(elem=> {
      var elemVar = elem - avg
      varianceValue += (elemVar*elemVar)
    })
    varianceValue / (count-1)
  }


  def standardDeviation: Double ={
    math.sqrt(variance)
  }

  def coefficientOfVariation: Double = {
    standardDeviation/avg
  }

  def createHistogram(delta: Double, times: Int): Unit = {
    elements.sortInPlace()
    var currBlock: Double = delta
    histogram = new Curve2D()
    histogram.append((currBlock,0))
    var inBlock:Double =0
    for (currentEntry <- elements.indices) {
      val entry: Double = elements(currentEntry)
      if(entry > currBlock) {
        currBlock += delta
        histogram.append((currBlock,inBlock))
        inBlock=0
      }else {
        inBlock+=1
      }
    }
    histogram.append((currBlock,inBlock))
    for (i <- histogram.length to times) {
      currBlock += delta
      histogram.append((currBlock,0))
    }
  }



}
