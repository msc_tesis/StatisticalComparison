import java.io.{File, FileWriter}
import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.io.Source

object Main {
  def main(args: Array[String]): Unit = {
    val roundsEntry: String = "rounds"
    val coinsEntry: String = "coins"
    val tilesEntry: String = "tiles"
    val entries: mutable.Map[String, Double] = mutable.Map[String, Double]()
    val sources: mutable.Map[String, String] = mutable.Map[String, String]()
    entries += (roundsEntry -> 100)
    entries += (coinsEntry -> 2.5)
    entries += (tilesEntry -> 2.5)
    sources += ("Netlogo" -> "../results/pareto.netlogo.csv")
    sources += ("Mason" -> "../results/pareto.mason.csv")
    sources += ("Repast" -> "../results/pareto.repast.csv")
    sources += ("Mesa" -> "../results/pareto.mesa.csv")
    sources += ("Agents.jl" -> "../results/pareto.agents.jl.csv")
    sources += ("AB-X" -> "../results/pareto.ab-x.csv")
    val dataContainer = new MacroValuesContainer(entries)

    sources.foreach(entry => {
      val (framework: String, csvFile: String) = entry
      dataContainer.addFramework(framework)

      val src = Source.fromFile(csvFile)
      val iter = src.getLines().map(_.split(","))
      iter.foreach(a => {
        dataContainer.add(framework, roundsEntry, a(0).toDouble)
        dataContainer.add(framework, coinsEntry, a(1).toDouble)
        dataContainer.add(framework, tilesEntry, a(2).toDouble)
      })
      src.close()
    })

    entries.keys.foreach(entryName => {
      println(s"Entry $entryName ")
      println(s"  Global max ${dataContainer.maxValues(entryName)}")
    })

    sources.keys.foreach(framework => {
      println(s"Framework $framework")
      entries.keys.foreach(entryName => {
        println(s"  Entry $entryName ")
        val frameworkContainer: FrameworkContainer = dataContainer.frameworksContainer(framework)
        println(s"    Min ${frameworkContainer.entries(entryName).min}")
        println(s"    Max ${frameworkContainer.entries(entryName).max}")
        println(s"    Mag ${frameworkContainer.entries(entryName).avg}")
        println(s"    Variance ${frameworkContainer.entries(entryName).variance}")
        println(s"    Standard Deviation ${frameworkContainer.entries(entryName).standardDeviation}")
        println(s"    Coefficient of Variation ${frameworkContainer.entries(entryName).coefficientOfVariation}")
      })
    })
    dataContainer.createHistogram()

    entries.keys.foreach(entry => {
      var curr: Double = 0
      val incr: Double = dataContainer.incrValues(entry)
      val fileWriter = new FileWriter(new File(s"target/$entry.csv"))
      var text = entry + ","
      sources.keys.foreach(f = framework => {
        text += (framework + ",")
      })
      text += "\n"
      for (i <- 0 to dataContainer.totalEntries(entry)) {
        text += (curr + ",")
        curr += incr
        sources.keys.foreach(framework => {
          text += dataContainer.frameworksContainer(framework).getHistogram(entry, i)._2
          text += ","
        })
        text += "\n"
      }
      fileWriter.write(text)
      fileWriter.close()
    })

    entries.keys.foreach(entry => {
      println(entry)
      println(dataContainer.frechet(entry))
    })
    for (entry <- entries.keys) {
      val sb: StringBuilder = new StringBuilder()
      sb.append(entry)
      sb.append("\n")
      sb.append(dataContainer.anovaP(entry, dataContainer.frameworksContainer.values))
      sb.append("\n")
      val sortedColumns = new ArrayBuffer[String]()
      sortedColumns.addAll(dataContainer.frameworksContainer.keys)
      sortedColumns.sortInPlace()
      sb.append("&")
      for (containerA <- sortedColumns) {
        sb.append(s" $containerA &")
      }
      sb.deleteCharAt(sb.length - 1)
      sb.append("\\\\\n")
      for (nameA <- sortedColumns) {
        val frameworkContainerA: FrameworkContainer = dataContainer.frameworksContainer(nameA)
        sb.append(s"$nameA &")
        for (nameB <- sortedColumns) {
          if (nameA == nameB) {
            sb.append(" - &")

          } else {
            val frameworkContainerB: FrameworkContainer = dataContainer.frameworksContainer(nameB)
            val frameworks: ArrayBuffer[FrameworkContainer] = new ArrayBuffer[FrameworkContainer]()
            frameworks.append(frameworkContainerA)
            frameworks.append(frameworkContainerB)
            val pValue: Double = dataContainer.anovaP(entry, frameworks)
            if (pValue < 0.0005) {
              sb.append(" $<10^{-5}$")
            } else {
              sb.append(" %.3f".format(pValue))
            }
            if (pValue < 0.001) {
              sb.append("*** &")
            } else if (pValue < 0.01) {
              sb.append("** &")
            } else if (pValue < 0.05) {
              sb.append("* &")
            } else {
              sb.append(" &")
            }
          }
        }
        sb.deleteCharAt(sb.length - 1)
        sb.append("\\\\\n")
      }

      println(sb.toString())
    }


  }
}
