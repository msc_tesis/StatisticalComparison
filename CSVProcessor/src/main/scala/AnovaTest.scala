import org.apache.commons.math3.stat.inference.OneWayAnova
import java.util

object AnovaTest{

  def main(args: Array[String]): Unit = {
    // Example data
    val group1Data = Array(31.0, 31.0, 31.0, 31.0, 31.0)
    val group2Data = Array(31.0, 31.0, 31.0, 32.0, 33.0)
    val group3Data = Array(31.0, 31.0, 31.0, 31.0, 31.0)

    // Create an array of arrays for data
    val dataArray:util.ArrayList[Array[Double]] = new java.util.ArrayList[Array[Double]]()
    dataArray.add(group1Data)
    dataArray.add(group2Data)
    dataArray.add(group3Data)

    // Initialize OneWayAnova object and perform ANOVA
    val anova = new OneWayAnova()

    val pValue = anova.anovaPValue(dataArray)
    // Output results
    println(s"P-value: $pValue")
    if (pValue < 0.05) {
      println("Reject null hypothesis, means are significantly different.")
    } else {
      println("Fail to reject null hypothesis, means are not significantly different.")
    }
  }
}
