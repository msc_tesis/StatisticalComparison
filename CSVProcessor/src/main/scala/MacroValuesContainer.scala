import org.apache.commons.math3.stat.inference.OneWayAnova

import java.util
import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.math._

class MacroValuesContainer(entries: mutable.Map[String, Double]) {
  val frameworksContainer: mutable.Map[String, FrameworkContainer] = mutable.Map[String, FrameworkContainer]()

  val maxValues: mutable.Map[String, Double] = mutable.Map[String, Double]()
  var incrValues: mutable.Map[String, Double] = mutable.Map[String, Double]()
  var totalEntries: mutable.Map[String, Int] = mutable.Map[String, Int]()

  entries.foreach(entry => {
    val (entryName, incrValue) = entry
    incrValues += (entryName -> incrValue)
    maxValues += (entryName -> Double.MinValue)
  })

  def addFramework(frameworkName: String): Unit = {
    frameworksContainer += (frameworkName -> new FrameworkContainer(entries.keys.toArray))
  }


  def frechet(entryName: String): NamedMatrix[Double] = {
    val matrix: NamedMatrix[Double] = new NamedMatrix[Double](0)
    frameworksContainer.foreach {
      case (frameworkNameA, frameworkContainerA) =>
        frameworksContainer.foreach {
          case (frameworkNameB, frameworkContainerB) =>
            val entryContainerA: EntryContainer = frameworkContainerA.entries(entryName)
            val entryContainerB: EntryContainer = frameworkContainerB.entries(entryName)
            matrix(frameworkNameA, frameworkNameB) =
              max(entryContainerA.histogram.frechet(
                entryContainerB.histogram
              ), entryContainerB.histogram.frechet(
                entryContainerA.histogram
              ));
        }

    }
    matrix
  }


  def add(frameworkName: String, entryName: String, entryValue: Double): Unit = {
    maxValues.update(entryName, Math.max(entryValue, maxValues(entryName)))
    frameworksContainer(frameworkName).add(entryName, entryValue)
  }

  def createHistogram(): Unit = {
    for (entryName <- incrValues.keys) {
      val incr: Double = incrValues(entryName)
      val max: Double = maxValues(entryName)
      val times: Int = Math.ceil(max / incr).toInt
      totalEntries += (entryName -> times)

      for (frameworkContainer <- frameworksContainer.values) {
        frameworkContainer.entries(entryName).createHistogram(incr, times)
      }
    }
  }

  def anova(entry: String): (Double,Int,Int) = {
    anova(entry, frameworksContainer.values)
  }

  def anovaP(entry: String, frameworkContainers: Iterable[FrameworkContainer]):Double ={
    val dataArray:util.ArrayList[Array[Double]] = new util.ArrayList[Array[Double]]()
    for (frameworkContainer <- frameworkContainers) {
      val entryContainer: EntryContainer = frameworkContainer.entries(entry)
      dataArray.add(entryContainer.elements.toArray[Double])
    }
    new OneWayAnova().anovaPValue(dataArray)
  }

  def anova(entry: String, frameworkContainers: Iterable[FrameworkContainer]):  (Double,Int,Int) = {
    val frameworkMean: ArrayBuffer[(Double, Int)] = new ArrayBuffer[(Double, Int)]()
    var overalAvgSum: Double = 0
    for (frameworkContainer <- frameworkContainers) {
      var entryContainer: EntryContainer = frameworkContainer.entries(entry)
      val frameworkAvg: Double = entryContainer.avg
      val frameworkLength: Int = entryContainer.total
      overalAvgSum += frameworkAvg
      frameworkMean.append((frameworkAvg, frameworkLength))
    }
    val overallAvg: Double = overalAvgSum / frameworkContainers.size
    var betweenGroup: Double = 0
    for (frameworkAverage <- frameworkMean) {
      val (frameworkAvg: Double, frameworkCount: Int) = frameworkAverage
      val frameworkStdDev = frameworkAvg - overallAvg
      betweenGroup += frameworkCount * (frameworkStdDev * frameworkStdDev)
    }
    val betweenGroupDegreesOfFreedom = frameworksContainer.size - 1
    val betweenGroupMeanSquare = betweenGroup / betweenGroupDegreesOfFreedom

    var withinGroupSumOfSquares: Double = 0
    var withinGroupDegreeOfFreedom: Int = 0
    for (frameworkContainer <- frameworkContainers) {
      val entryContainer: EntryContainer = frameworkContainer.entries(entry)
      val frameworkAvg = entryContainer.avg
      withinGroupDegreeOfFreedom += entryContainer.total - 1
      for (entry <- entryContainer.elements) {
        val entryStdDev = entry - frameworkAvg
        withinGroupSumOfSquares += entryStdDev * entryStdDev
      }
    }
    val withinGroupMeanSquareValue: Double = withinGroupSumOfSquares / withinGroupDegreeOfFreedom
    val F = betweenGroupMeanSquare / withinGroupMeanSquareValue
    (F, betweenGroupDegreesOfFreedom, withinGroupDegreeOfFreedom)
  }

}
