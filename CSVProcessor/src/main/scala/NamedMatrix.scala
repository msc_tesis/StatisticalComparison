
import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

class NamedMatrix[T](theDefault: T) {

  private val default: T = theDefault
  val values: mutable.Map[String, mutable.Map[String, T]] = mutable.Map[String, mutable.Map[String, T]]()

  def update(a: String, b: String, value: T): Unit = {
    if (!values.contains(a)) {
      values += a -> mutable.Map[String, T]()
    }
    values(a) += b -> value;
  }

  def apply(a: String, b: String): T = {
    if (!values.contains(a)) {
      values += a -> mutable.Map[String, T]()
    }
    val thisMap: mutable.Map[String, T] = values(a);
    if (!thisMap.contains(b)) {
      thisMap += b -> default
    }
    thisMap(b);
  }

  def +=(x: ((String, String), T)): Unit = {
    val ((a: String, b: String), value: T) = x
    if (!values.contains(a)) {
      values += a -> mutable.Map[String, T]()
    }
    values(a) += b -> value;
  }

  override def toString: String = {
    val sb: StringBuilder = new StringBuilder("Matrix,")
    val sortedRow = new ArrayBuffer[String]()
    val columns = mutable.Set[String]()
    values.foreach(column => {
      columns.addAll(column._2.keys)
    });
    val sortedColumns = new ArrayBuffer[String]()
    sortedColumns.addAll(columns)
    sortedColumns.sortInPlace()
    sortedRow.addAll(values.keys).sortInPlace()
    sortedColumns.foreach(nameA => {
      sb ++= nameA
      sb ++= " & "
    });
    sb.deleteCharAt(sb.length - 1)
    sb += '\n'
    sortedRow.foreach(nameA => {
      sb ++= nameA
      sb ++= " & "
      sortedColumns.foreach(nameB => {
        val value: T = this (nameA, nameB)
        sb ++= value.toString
        sb ++= " & "
      });
      sb.deleteCharAt(sb.length - 1)
      sb.deleteCharAt(sb.length - 1)
      sb ++= "\\\\\n"
    })
    sb.toString
  }
}

object Main2 {
  def main(args: Array[String]): Unit = {
    var matrix: NamedMatrix[Double] = new NamedMatrix[Double](0);
    matrix("a", "b") = 4;
    matrix += ("b", "c") -> 45;
    println(matrix("a", "b"))
    println(matrix("b", "c"))
    println(matrix("a", "c"))
    println(matrix)
  }
}
