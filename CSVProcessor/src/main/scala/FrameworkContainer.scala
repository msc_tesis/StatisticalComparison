import scala.collection.mutable

class FrameworkContainer(entriesName: Array[String]) {
  val entries: mutable.Map[String, EntryContainer] = mutable.Map[String, EntryContainer]()
  //Setup process
  entriesName.foreach(entryName => {
    entries.put(entryName, new EntryContainer())
  })

  def add(entryName: String, entryValue: Double): Unit = {
    entries(entryName).addValue(entryValue)
  }

  def getHistogram(entryName: String, column: Int): (Double,Double) = {
    entries(entryName).histogram.pointAt(column)
  }


}
